COMP1005 2023 Tutorial 2
========================


## Background

* [Conditionals](https://www.tutorialspoint.com/cprogramming/c_decision_making.htm)
* [Loops](https://www.tutorialspoint.com/cprogramming/c_loops.htm)
    * [For loops](https://www.learn-c.org/en/For_loops)
    * [While loops](https://www.learn-c.org/en/While_loops)

## Exercises

0. Write a C program that accepts three integers and find the maximum.  
    ***Expected Output:*** 
    ```
    Input the first integer: 25 
    Input the second integer: 35 
    Input the third integer: 15 
    Maximum value of three integers: 35 
    ```

0. Write a C program to find whether a given year is a leap year or not.  
    ***Expected Output:*** 
    ```
    Input the year: 2016 
    2016 is a leap year.
    ```

0. Write a C program to check a given integer is positive even, negative even, positive odd or negative odd. Print even if the number is 0.  
    ***Expected Output:*** 
    ```
    Input an integer: 13
    Positive Odd
    ```

0. Write a program in C to display n terms of natural number and their sum.  
    ***Expected Output:*** 
    ```
    Input an integer (>0): 7
    The first 7 natural number is : 
    1 2 3 4 5 6 7 
    The Sum of Natural Number upto 7 terms : 28 
    ```

0. Write a C program to calculate the factorial of a given number.  
    ***Expected Output:*** 
    ```
    Input the number: 5
    The Factorial of 5 is: 120 
    ```


## Extras

0. Write a C program to calculate the root of a Quadratic Equation.  
    <img src="https://www.w3resource.com/w3r_images/c-conditional-statement-image-exercises-11.png" width="128">  
    ***Expected Output:*** 
    ```
    Input a, b and c: 1 5 7 
    No solution for this Quadratic Equation.
    ```

0. Write a C program to accept a coordinate point in a XY coordinate system and determine in which quadrant the coordinate point lies.  
   ***Expected Output:*** 
   ```
   Input a coordinate point: 7,9
   The coordinate point (7,9) lies in the First quadrant.
   ```

0. Write a program in C to make such a pattern like a pyramid with numbers increased by 1.  
   ***Expected Output:*** 
   ```
   Input the number of rows (<=10): 4
      1 
     2 3 
    4 5 6 
   7 8 9 10 
   ```
   
0. Write a program in C to display the pattern like a diamond.  
   ***Expected Output:*** 
   ```
   Input the number of rows (<=10): 5
        *
       *** 
      *****
     *******
    ********* 
     *******
      *****
       ***
        *
   ```

0. Try different operators (including bitwise operators) to see how they work.
